#!/bin/bash

function createIcesi() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/peerOrganizations/icesi.universidades.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/icesi.universidades.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-icesi --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-icesi.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-icesi.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-icesi.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-icesi.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/peerOrganizations/icesi.universidades.com/msp/config.yaml"

  infoln "Registering peer0"
  set -x
  fabric-ca-client register --caname ca-icesi --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering user"
  set -x
  fabric-ca-client register --caname ca-icesi --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the org admin"
  set -x
  fabric-ca-client register --caname ca-icesi --id.name icesiadmin --id.secret icesiadminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-icesi -M "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/msp" --csr.hosts peer0.icesi.universidades.com --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/msp/config.yaml"

  infoln "Generating the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-icesi -M "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls" --enrollment.profile tls --csr.hosts peer0.icesi.universidades.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/ca.crt"
  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/signcerts/"* "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/server.crt"
  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/keystore/"* "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/server.key"

  mkdir -p "${PWD}/organizations/peerOrganizations/icesi.universidades.com/msp/tlscacerts"
  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/icesi.universidades.com/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/organizations/peerOrganizations/icesi.universidades.com/tlsca"
  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/icesi.universidades.com/tlsca/tlsca.icesi.universidades.com-cert.pem"

  mkdir -p "${PWD}/organizations/peerOrganizations/icesi.universidades.com/ca"
  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/peers/peer0.icesi.universidades.com/msp/cacerts/"* "${PWD}/organizations/peerOrganizations/icesi.universidades.com/ca/ca.icesi.universidades.com-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-icesi -M "${PWD}/organizations/peerOrganizations/icesi.universidades.com/users/User1@icesi.universidades.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/icesi.universidades.com/users/User1@icesi.universidades.com/msp/config.yaml"

  infoln "Generating the org admin msp"
  set -x
  fabric-ca-client enroll -u https://icesiadmin:icesiadminpw@localhost:7054 --caname ca-icesi -M "${PWD}/organizations/peerOrganizations/icesi.universidades.com/users/Admin@icesi.universidades.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/icesi/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/icesi.universidades.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/icesi.universidades.com/users/Admin@icesi.universidades.com/msp/config.yaml"
}

function createUao() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/peerOrganizations/uao.universidades.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/uao.universidades.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-uao --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-uao.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-uao.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-uao.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-uao.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/peerOrganizations/uao.universidades.com/msp/config.yaml"

  infoln "Registering peer0"
  set -x
  fabric-ca-client register --caname ca-uao --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering user"
  set -x
  fabric-ca-client register --caname ca-uao --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the org admin"
  set -x
  fabric-ca-client register --caname ca-uao --id.name uaoadmin --id.secret uaoadminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-uao -M "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/msp" --csr.hosts peer0.uao.universidades.com --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/msp/config.yaml"

  infoln "Generating the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-uao -M "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls" --enrollment.profile tls --csr.hosts peer0.uao.universidades.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/ca.crt"
  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/signcerts/"* "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/server.crt"
  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/keystore/"* "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/server.key"

  mkdir -p "${PWD}/organizations/peerOrganizations/uao.universidades.com/msp/tlscacerts"
  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/uao.universidades.com/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/organizations/peerOrganizations/uao.universidades.com/tlsca"
  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/uao.universidades.com/tlsca/tlsca.uao.universidades.com-cert.pem"

  mkdir -p "${PWD}/organizations/peerOrganizations/uao.universidades.com/ca"
  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/peers/peer0.uao.universidades.com/msp/cacerts/"* "${PWD}/organizations/peerOrganizations/uao.universidades.com/ca/ca.uao.universidades.com-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-uao -M "${PWD}/organizations/peerOrganizations/uao.universidades.com/users/User1@uao.universidades.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/uao.universidades.com/users/User1@uao.universidades.com/msp/config.yaml"

  infoln "Generating the org admin msp"
  set -x
  fabric-ca-client enroll -u https://uaoadmin:uaoadminpw@localhost:8054 --caname ca-uao -M "${PWD}/organizations/peerOrganizations/uao.universidades.com/users/Admin@uao.universidades.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/uao/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/uao.universidades.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/uao.universidades.com/users/Admin@uao.universidades.com/msp/config.yaml"
}

function createOrderer() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/ordererOrganizations/universidades.com

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/universidades.com

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-orderer --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/ordererOrganizations/universidades.com/msp/config.yaml"

  infoln "Registering orderer"
  set -x
  fabric-ca-client register --caname ca-orderer --id.name orderer --id.secret ordererpw --id.type orderer --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the orderer admin"
  set -x
  fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the orderer msp"
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:9054 --caname ca-orderer -M "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/msp" --csr.hosts orderer.universidades.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/universidades.com/msp/config.yaml" "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/msp/config.yaml"

  infoln "Generating the orderer-tls certificates"
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:9054 --caname ca-orderer -M "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls" --enrollment.profile tls --csr.hosts orderer.universidades.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/ca.crt"
  cp "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/signcerts/"* "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/server.crt"
  cp "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/keystore/"* "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/server.key"

  mkdir -p "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/msp/tlscacerts"
  cp "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/msp/tlscacerts/tlsca.universidades.com-cert.pem"

  mkdir -p "${PWD}/organizations/ordererOrganizations/universidades.com/msp/tlscacerts"
  cp "${PWD}/organizations/ordererOrganizations/universidades.com/orderers/orderer.universidades.com/tls/tlscacerts/"* "${PWD}/organizations/ordererOrganizations/universidades.com/msp/tlscacerts/tlsca.universidades.com-cert.pem"

  infoln "Generating the admin msp"
  set -x
  fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@localhost:9054 --caname ca-orderer -M "${PWD}/organizations/ordererOrganizations/universidades.com/users/Admin@universidades.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/universidades.com/msp/config.yaml" "${PWD}/organizations/ordererOrganizations/universidades.com/users/Admin@universidades.com/msp/config.yaml"
}
